
import argparse
import time
import os
import requests
import json
import random

interval = 5
keep = False
keep_path = os.path.expanduser("~") +"/.rms-background/"
origin = "https://rms.sexy"
debug = False

print("This program sets your background to pictures of Richard Stallman using feh, pass -h to see usage.")
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--interval", help="Time frame in secounds after background is changed, default is 30 secs", dest="interval", type=int)
parser.add_argument("-k", "--keep", help="Pass this argument if you want to save pictures, default location is: "+ keep_path, action="store_true", dest="keep")
parser.add_argument("-p", "--path", help="If option -k is passed, you can specify directory for saving sexy backgrounds if you dont want default one, ex: "+os.path.expanduser("~")+"/Pictures/porn", dest="keep_path")
parser.add_argument("-d", "--debug", action="store_true", dest="debug")

args = parser.parse_args()


if debug:
	print("Inteval is:" + str(interval))
	print("Keep images: " + str(keep))
	print("Keep path: " + str(keep_path))
	print("Origin: " + origin)
	print("Debug: well, yes")

if not os.path.exists(keep_path):
    os.makedirs(keep_path)

r = requests.get(origin+"/?images")

images = json.loads(r.text)

while True:
	img = random.choice(images)
	r = requests.get(origin+img)
	img_path = keep_path+img.rsplit('/', 1)[1]
	open(img_path, 'wb').write(r.content)
	os.system("feh --bg-scale " + img_path)
	print("Bg is set to " + img_path)
	time.sleep(interval)
